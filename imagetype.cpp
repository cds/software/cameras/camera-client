/* Patrick J. Thomas, California Institute of Technology, LIGO Hanford */


#include "imagetype.h"

#include <QPainter>

void ImageType::setData(const QPixmap& pixmap) {
  this->pixmap = pixmap;

  this->repaint();
}

void ImageType::setSnapshot() {
  this->snapshot = this->pixmap;
}

void ImageType::saveSnapshot(QString filename) {
  this->snapshot.save(filename, "PNG");
}

void ImageType::paintEvent(QPaintEvent* event) {
  (void) event;
  QPainter painter(this);
  QPixmap scaled;


  if (!pixmap.isNull()) {
    scaled = pixmap.scaled(this->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    painter.drawPixmap(0, 0, scaled);
  }
  else {
    painter.fillRect(this->rect(), QColor("blue"));
  }
}
