# camera-client

**H264 RTP video streaming client**

Connects to a UDP multicast address and port to display a H264 RTP video stream in a Qt graphical user interface. Provides a button to save a snapshot of the stream as an image file. A static blue screen is used to indicate a UDP timeout.

## Requirements

Debian packages:

### Build

- qtbase5-dev
- qtchooser
- qt5-qmake
- qtbase5-dev-tools
- libgstreamer-plugins-base1.0-dev

### Run

- gstreamer1.0-plugins-good
- gstreamer1.0-libav

## Usage

```
camera-client ADDRESS PORT LABEL [-t TIMEOUT] [-f FONT] [-s SIZE]

Required:
ADDRESS: The multicast group address
PORT: The multicast port
LABEL: The label text

Optional:
TIMEOUT: The UDP timeout (seconds)
FONT: The label font
SIZE: The label size
```

Example
```camera-client 239.192.106.60 5000 'MC REFL' -t 1 -f Courier -s 60```
