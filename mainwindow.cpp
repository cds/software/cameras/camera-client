/* Patrick J. Thomas, California Institute of Technology, LIGO Hanford */


#include "mainwindow.h"

MainWindow::MainWindow(QString label_text, QString label_font, qreal label_point_size) {
  QFont font;


  centralWidget = new QWidget;
  layout = new QVBoxLayout;
  labelWidget = new QLabel;
  imageWidget = new ImageType;
  buttonWidget = new QPushButton;
  dialogWidget = new QFileDialog;


  labelWidget->setText(label_text);
  font.setFamily(label_font);
  font.setPointSizeF(label_point_size);
  labelWidget->setFont(font);
  labelWidget->setAlignment(Qt::AlignHCenter);
  QSizePolicy labelWidget_size_policy(QSizePolicy::Preferred, QSizePolicy::Fixed);
  labelWidget->setSizePolicy(labelWidget_size_policy);

  layout->addWidget(labelWidget);


  layout->addWidget(imageWidget);


  QSizePolicy buttonWidget_size_policy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  buttonWidget->setSizePolicy(buttonWidget_size_policy);
  buttonWidget->setIcon(QIcon::fromTheme("camera-photo"));
  buttonWidget->setIconSize(QSize(24, 24));
  buttonWidget->setEnabled(false);

  layout->addWidget(buttonWidget);


  dialogWidget->setFileMode(QFileDialog::AnyFile);
  dialogWidget->setAcceptMode(QFileDialog::AcceptSave);
  dialogWidget->setNameFilter("Images (*.png)");
  dialogWidget->setDefaultSuffix(".png");


  QObject::connect(buttonWidget, SIGNAL(clicked()), imageWidget, SLOT(setSnapshot()));
  QObject::connect(buttonWidget, SIGNAL(clicked()), dialogWidget, SLOT(show()));
  QObject::connect(dialogWidget, SIGNAL(fileSelected(QString)), imageWidget, SLOT(saveSnapshot(QString)));


  centralWidget->setLayout(layout);

  setCentralWidget(centralWidget);

  setWindowTitle("GigE Camera - " + label_text);

  resize(500, 500);
}

void MainWindow::setData(const QPixmap& pixmap) {
  if (!pixmap.isNull()) {
    buttonWidget->setEnabled(true);
  }
  else {
    buttonWidget->setEnabled(false);
  }

  imageWidget->setData(pixmap);
}

