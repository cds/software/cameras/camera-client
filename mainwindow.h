/* Patrick J. Thomas, California Institute of Technology, LIGO Hanford */


#include "imagetype.h"

#include <QMainWindow>

#include <QVBoxLayout>

#include <QLabel>
#include <QPushButton>
#include <QFileDialog>

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QString label_text, QString label_font, qreal label_point_size);

public slots:
  void setData(const QPixmap& pixmap);

private:
  QVBoxLayout* layout;
  QLabel* labelWidget;
  ImageType* imageWidget;
  QPushButton* buttonWidget;
  QFileDialog* dialogWidget;
  QWidget* centralWidget;
};
