import argparse
import os

from qtpy import QtWidgets


parser = argparse.ArgumentParser()
parser.add_argument('address', type=str)
parser.add_argument('port', type=int)
args = parser.parse_args()

address = args.address
port = args.port

app = QtWidgets.QApplication([])
file_path, file_ext = QtWidgets.QFileDialog.getSaveFileName(filter='*.avi')

if file_path:
    print("\nTo stop recording, use CTRL-C")
    command = f'gst-launch-1.0 udpsrc port={port} address={address} ! application/x-rtp ! rtph264depay ! queue ! avdec_h264 ! videoconvert ! avimux ! filesink location={file_path}'
    print(command)
    os.system(command)
