/* Patrick J. Thomas, California Institute of Technology, LIGO Hanford */


#include <QWidget>

class ImageType : public QWidget
{
  Q_OBJECT

public:
  ImageType(QWidget* parent = nullptr) : QWidget(parent) {};
  void setData(const QPixmap& pixmap);
  void paintEvent(QPaintEvent* event);

public slots:
  void setSnapshot();
  void saveSnapshot(QString filename);

private:
  QPixmap pixmap;
  QPixmap snapshot;
};
