/* Patrick J. Thomas, California Institute of Technology, LIGO Hanford */


#include <gst/gst.h>
#include <gst/app/gstappsink.h>

#include <QObject>

#include <QPixmap>

class AcquisitionType;

struct gstreamer_data {
  GstElement* udpsrc;
  GstElement* rtph264depay;
  GstElement* avdec_h264;
  GstElement* videoconvert;
  GstElement* tee;

  GstElement* appsink;

  GstElement* filesink_valve;
  GstElement* filesink;

  GstElement* pipeline;

  gulong signal_handler_id;

  AcquisitionType* instance;
};


class AcquisitionType : public QObject
{
  Q_OBJECT

public:
  AcquisitionType(char const* address, gint port, guint64 udp_timeout);
  ~AcquisitionType();

signals:
  void setData(const QPixmap&);

private:
  static GstFlowReturn new_sample_callback(GstAppSink* appsink, gstreamer_data* user_data);
  static GstPadProbeReturn udpsrc_buffer_pad_probe_callback(GstPad* pad, GstPadProbeInfo* info, gstreamer_data* user_data);
  static void udpsrc_timeout_callback(GstBus* bus, GstMessage* message, gstreamer_data* user_data);
  static void bus_error_callback(GstBus* bus, GstMessage* message, gstreamer_data* user_data);

  gstreamer_data data;
};

